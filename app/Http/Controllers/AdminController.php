<?php

namespace App\Http\Controllers;

use App\oncesi;
use App\service;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\ekim_category;
use App\tedavi_category;
use App\sakal_biyik;
use App\kas_category;
use App\social;
use App\appointment;
use App\message;


class AdminController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth:admin');
        $this->msgcount=message::where('status',0)->get()->count();
    }

    public function panel(){
        $sayi['randevu']=appointment::count();

        return view('admin/admin_panel')->with('sayi',$sayi)->with('msgcount',$this->msgcount);
    }

    public function sac_ekimi(){
        $sub=ekim_category::get();
        return view('admin/duzenlemeler/sac_ekimi')->with('sub',$sub)->with('msgcount',$this->msgcount);
    }
    public function sacekimi(Request $req){
        $sac=new service();
        $sac->aciklama=$req->aciklama;
        $sac->kimler_icin=$req->kimler_icin;
        $sac->sure=$req->sure;
        $sac->seans_sayi=$req->seans_sayi;
        $sac->servis_sonrası=$req->servis_sonrası;
        $sac->keywords=$req->keywords;
        $sac->category="sac-ekimi";
        $sac->sub_category=$req->sub_category;
        if($sac->save()){

            return redirect()->back()->with('status','Başarıyla Kaydedildi');
        }
    }

    public function sacekimi_cat(Request $req){
        $cat=new ekim_category();
        $cat->category=$req->category;
        $cat->category_desc=$req->category_desc;
        $cat->path=$req->path;

        if($cat->save()){
            return redirect()->back()->with('status','Başarıyla Kaydedildi');
        }

    }

    public function sac_dokulme_tedavi(){
        $sub=tedavi_category::get();
        return view('admin/duzenlemeler/sac_dokulme_tedavi')->with('sub',$sub);
        
    }
    public function sacdokulmetedavi(Request $req){
        $td=new service();
        $td->aciklama=$req->aciklama;
        $td->kimler_icin=$req->kimler_icin;
        $td->sure=$req->sure;
        $td->seans_sayi=$req->seans_sayi;
        $td->servis_sonrası=$req->servis_sonrası;
        $td->keywords=$req->keywords;
        $td->category="sac-dokulme-tedavi";
        $td->sub_category=$req->sub_category;
        if($td->save()){

            return redirect()->back()->with('status','Hizmet Açıklaması Başarıyla Kaydedildi');
        }
    }
    public function sacdokulmetedavi_cat(Request $req){
        $cat=new tedavi_category();
        $cat->category=$req->category;
        $cat->category_desc=$req->category_desc;
        $cat->path=$req->path;

        if($cat->save()){
            return redirect()->back()->with('status','Kategori Başarıyla Kaydedildi');
        }

    }

    public function sakal_biyik(){
        $sub=sakal_biyik::get();
        return view('admin/duzenlemeler/sac_biyik')->with('sub',$sub)->with('msgcount',$this->msgcount);

    }
    public function sakalbiyik(Request $req){
        $td=new service();
        $td->aciklama=$req->aciklama;
        $td->kimler_icin=$req->kimler_icin;
        $td->sure=$req->sure;
        $td->seans_sayi=$req->seans_sayi;
        $td->servis_sonrası=$req->servis_sonrası;
        $td->keywords=$req->keywords;
        $td->category="sakal-biyik";
        $td->sub_category=$req->sub_category;
        if($td->save()){

            return redirect()->back()->with('status','Hizmet Açıklaması Başarıyla Kaydedildi');
        }
    }

    public function sakalbiyik_cat(Request $req){
        $cat=new sakal_biyik();
        $cat->category=$req->category;
        $cat->category_desc=$req->category_desc;
        $cat->path=$req->path;

        if($cat->save()){
            return redirect()->back()->with('status','Kategori Başarıyla Kaydedildi');
        }

    }


    public function kas_ekimi(){
        $sub=kas_category::get();
        return view('admin/duzenlemeler/kas_ekimi')->with('sub',$sub)->with('msgcount',$this->msgcount);
    }
    public function kasekimi(Request $req){
        $td=new service();
        $td->aciklama=$req->aciklama;
        $td->kimler_icin=$req->kimler_icin;
        $td->sure=$req->sure;
        $td->seans_sayi=$req->seans_sayi;
        $td->servis_sonrası=$req->servis_sonrası;
        $td->keywords=$req->keywords;
        $td->category="kas-ekimi";
        $td->sub_category=$req->sub_category;
        if($td->save()){

            return redirect()->back()->with('status','Hizmet Açıklaması Başarıyla Kaydedildi');
        }
    }

    public function kasekimi_cat(Request $req){
        $cat=new kas_category();
        $cat->category=$req->category;
        $cat->category_desc=$req->category_desc;
        $cat->path=$req->path;

        if($cat->save()){
            return redirect()->back()->with('status','Kategori Başarıyla Kaydedildi');
        }

    }

    public function oncesi_sonrasi(){
        $sub=oncesi::get();
        return view('admin/duzenlemeler/oncesi-sonrasi')->with('sub',$sub)->with('msgcount',$this->msgcount);
    }
    public function oncesisonrasi(Request $req){
        $td=new service();
        $td->aciklama=$req->aciklama;
        $td->kimler_icin=$req->kimler_icin;
        $td->sure=$req->sure;
        $td->seans_sayi=$req->seans_sayi;
        $td->servis_sonrası=$req->servis_sonrası;
        $td->keywords=$req->keywords;
        $td->category="oncesi-sonrasi";
        $td->sub_category=$req->sub_category;
        if($td->save()){

            return redirect()->back()->with('status','Hizmet Açıklaması Başarıyla Kaydedildi');
        }
    }

    public function oncesisonrasi_cat(Request $req){
        $cat=new oncesi();
        $cat->category=$req->category;
        $cat->category_desc=$req->category_desc;
        $cat->path=$req->path;

        if($cat->save()){
            return redirect()->back()->with('status','Kategori Başarıyla Kaydedildi');
        }

    }





    public function sosyal_medya(){
        $sosyal=social::find(1);
        return view('admin/duzenlemeler/sosyal_medya')->with('social',$sosyal)->with('msgcount',$this->msgcount);
    }
    public function sosyalmedya(Request $req){
        $sosyal=social::find(1);
        $sosyal->facebook=$req->facebook;
        $sosyal->twitter=$req->twitter;
        $sosyal->instagram=$req->instagram;

        if($sosyal->save()){
            return redirect()->back();
        }
    }


    public function blog(){
        return view('admin.blog');
    }

    public function randevular(){
        $randevular=appointment::get();
        return view('admin.randevular')->with('randevu',$randevular)->with('msgcount',$this->msgcount);
    }
    public function mesajlar(){
        $mesajlar=message::get();
        $okunmayan=message::where('status',0)->get();

        return view('admin.mesajlar')->with('mesajlar',$mesajlar)->with('okunmayan',$okunmayan)->with('msgcount',$this->msgcount);
    }


}
