<?php

namespace App\Http\Controllers;

use App\message;
use Illuminate\Http\Request;
use App\ekim_category;
use App\tedavi_category;
use App\service;
use App\sorular;
use App\sakal_biyik;
use App\kas_category;
use App\oncesi;
use App\social;
use App\appointment;
use App\subscribe;


use Illuminate\Support\Facades\Auth;

class MainController extends Controller
{

    public function __construct()
    {

        $this->middleware('guest');

        $this->menu['sac_ekimi']=ekim_category::get();
        $this->menu['tedavi']=tedavi_category::get();
        $this->menu['sakal_biyik']=sakal_biyik::get();
        $this->menu['kas']=kas_category::get();
        $this->menu['oncesi']=oncesi::get();
        $this->menu['social']=social::first();
    }

    public function login(){



        return view('admin/login')->with('menu',$this->menu);
    }
    public function logmein(Request $req){
        $this->validate($req,[
            'email' => 'required|email',
            'password' => 'required'
        ]);
        $result=Auth::guard('admin')->attempt(['email' => $req->email,'password' => $req->password],false);
        if($result){
           return redirect()->intended('admin/panel');
        }
        else{return 'dsada';}


    }
    public function index(){

        return view('main')->with('menu',$this->menu);
    }
    public function hizmet_detay(Request $req){
        if($req->category=='sac-ekimi'){

            if(!empty(ekim_category::where('path',$req->sub_category)->first())) {
                $sub = ekim_category::where('path', $req->sub_category)->first();
            }else {return redirect()->to('/');}

            $hizmet=service::where('sub_category',$sub->id)->where('category',$req->category)->first();

            $sss=sorular::where('category',$req->category)->where('sub_category',$sub->id)->get();


        }
        elseif($req->category=='sakal-biyik'){
            if(!empty(sakal_biyik::where('path',$req->sub_category)->first())) {
                $sub = sakal_biyik::where('path', $req->sub_category)->first();
            }else {return redirect()->to('/');}

            $hizmet=service::where('sub_category',$sub->id)->where('category',$req->category)->first();
            $sss=sorular::where('category',$req->category)->where('sub_category',$sub->id)->get();
        }

        elseif ($req->category=='sac-dokulme-tedavi'){

            if(!empty(tedavi_category::where('path',$req->sub_category)->first())) {
                $sub = tedavi_category::where('path', $req->sub_category)->first();
            }else {return redirect()->to('/');}

            $hizmet=service::where('sub_category',$sub->id)->where('category',$req->category)->first();
            $sss=sorular::where('category',$req->category)->where('sub_category',$sub->id)->get();


        }

        elseif ($req->category=='kas-ekimi'){

            if(!empty(kas_category::where('path',$req->sub_category)->first())) {
                $sub = kas_category::where('path', $req->sub_category)->first();
            }else {return redirect()->to('/');}

            $hizmet=service::where('sub_category',$sub->id)->where('category',$req->category)->orderBy('created_at','desc')->first();
            $sss=sorular::where('category',$req->category)->where('sub_category',$sub->id)->get();


        }

        elseif ($req->category=='oncesi-sonrasi'){

            if(!empty(oncesi::where('path',$req->sub_category)->first())) {
                $sub = oncesi::where('path', $req->sub_category)->first();
            }else {return redirect()->to('/');}

            $hizmet=service::where('sub_category',$sub->id)->where('category',$req->category)->orderBy('created_at','desc')->first();
            $sss=sorular::where('category',$req->category)->where('sub_category',$sub->id)->get();


        }

        else {
            return redirect()->to('/');
        }

            return view('hizmet_detay')->with('hizmet',$hizmet)->with('menu',$this->menu)->with('sub',$sub)->with('sss',$sss);

    }

    public function hakkimizda(){
        return view('hakkimizda')->with('menu',$this->menu);
    }

    public function iletisim(){
        return 'iletişim';
    }
    public function online_islemler(){
        return view('online_islemler')->with('menu',$this->menu);
    }

    public function blog(){
        return view('blog')->with('menu',$this->menu);
    }


    public function appointment(Request $req){
        $this->validate($req,[
            'name' => 'required',
            'surname' => 'required',
            'phone' => 'required',
            'email' => 'required|email'
        ]);
        $appoint= new appointment();
        $appoint->name=$req->name;
        $appoint->surname=$req->surname;
        $appoint->phone=$req->phone;
        $appoint->email=$req->email;

        $appoint->ip_address=request()->ip();
        $appoint->date=$req->date;
        if(isset($req->subject)){
            $appoint->subject=$req->subject;
        }
        if($appoint->save()){
            return redirect()->back();
        }

    }

    public function messageSend(Request $req){
        $this->validate($req,[
           'title' => 'required',
            'message' => 'required',
            'email' => 'required',
            'phone' => 'required'
        ]);
        $msg=new message();
        $msg->title=$req->title;
        $msg->message=$req->message;
        $msg->email=$req->email;
        $msg->phone=$req->phone;
        if($msg->save()){
            return 'true';
        }
    }

    public function subscribe(Request $req){
        $this->validate($req,[
            'email'
        ]);
        $sub=new subscribe();
        $sub->email=$req->email;
        if($sub->save()){
            return 'true';
        }

    }



    public function logout(){
        Auth::guard('admin')->logout();
        return redirect()->to('/');
    }
}
