<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class service extends Model
{
    protected $fillable=[
        'aciklama',
        'kimler_icin',
        'sure',
        'seans_sayi',
        'servis_sonrasi',
        'keywords',
        'img',
        'category',
        'sub_category'
    ];
}
