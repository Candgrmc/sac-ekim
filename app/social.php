<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class social extends Model
{
    public $timestamps=false;

    protected $fillable=[
        'facebook',
        'instagram',
        'twitter'
    ];
}
