<html lang="en">

<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="CanDgrmc">
    <base href="http://localhost:8000">
    <title>İstanbul Saç Nakli</title>
    <!-- Bootstrap core CSS -->
    <link href="{{asset('css/bootstrap.min.css')}}" rel="stylesheet">
    <!-- WebFonts core CSS -->
    <link href="{{asset('css/fonts.css')}}" rel="stylesheet">
    <link href="{{asset('css/animsition.css')}}" rel="stylesheet">
    <!-- Simple Line Icons -->
    <link href="MegaNavbar/assets/plugins/simple-line-icons/simple-line-icons.css" rel="stylesheet">
    <!-- OWL -->
    <link href="css/owl.carousel.css" rel="stylesheet">
    <!-- REVOLUTION BANNER CSS SETTINGS -->
    <link rel="stylesheet" type="text/css" href="rs-plugin/css/settings.css" media="screen" />
    <!-- MegaNavbar-->
    <link rel="stylesheet" type="text/css" href="{{asset('MegaNavbar/assets/css/MegaNavbar.min.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('MegaNavbar/assets/css/skins/navbar-default.css')}}">
    <link rel="stylesheet" type="text/css" href="{{asset('MegaNavbar/assets/css/animation/animation.css')}}">
    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
    <!-- Goole fonts -->
    <link href="http://fonts.googleapis.com/css?family=Open+Sans:300italic,400,300,600,700,800" rel="stylesheet" type="text/css">
    <link rel="stylesheet" href="css/sweetalert.css">
    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>

    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>


    <![endif]-->
</head>

<body>

<div class="animsition">
    <div class="wrapper">
        <header id="header">
            <!-- Top Header Section Start -->
            <div class="top-bar bg-light hdden-xs">
                <div class="container">
                    <div class="row">
                        <div class="col-sm-6 list-inline list-unstyled no-margin hidden-xs">
                            <p class="no-margin">Sorularınız için :  <strong>+90 532 543 0 300 - +90 532 543 0 500</strong> </p>


                        </div>

                        <div class="btn-group pull-right col-sm-6">
                            @if(Auth::guard('admin')->check())
                                <a href="{{url('admin')}}" class="btn btn-default btn-wide pull-right" style="z-index: 100">Panel</a>
                                <a href="{{url('logout')}}" class="btn btn-default btn-wide pull-right" style="z-index: 100">Çıkış</a>

                            @else
                                <div class="pull-right">
                                    <a href="{{$menu['social']->facebook}}" style="padding: 2px;margin: 5px" target="_blank"><img
                                                src="{{asset('images/icons/facebook.png')}}" alt=""></a>
                                    <a href="{{$menu['social']->twitter}}" style="padding: 2px;margin: 5px" target="_blank"><img
                                                src="{{asset('images/icons/twitter.png')}}" alt=""></a>
                                    <a href="{{$menu['social']->instagram}}" style="padding: 2px;margin: 5px" target="_blank"><img
                                                src="{{asset('images/icons/instagram.png')}}" alt=""></a>
                                </div>

                            @endif




                        </div>
                    </div>
                </div>
            </div>
            <!-- /.top bar -->
            <!-- begin Logo and info -->
            <div class="container-fluid middle-bar hidden-xs" style="background-repeat: no-repeat">
                <div class="row">
                    <a href="{{url('/')}}" class="logo col-sm-3">
                        <img src="{{asset('images/istanbulpng1.png')}}" class="img-responsive" alt=""/>

                    </a>
                    <div class="col-sm-8 col-sm-offset-1 contact-info">
                        <ul class="nav nav-pills pull-right" style="list-style: none;">
                            <li class="nav"><a href="{{url('/')}}">Anasayfa</a></li>
                            <li><a href="{{url('/online_islemler')}}">Online İşlemler</a></li>
                            <li><a href="">Blog</a></li>
                            <li><a href="{{url('hakkimizda')}}">Hakkımızda</a></li>
                            <li><a href="{{url('iletisim')}}">İletişim</a></li>
                        </ul>
                    </div>
                </div>
            </div>
            <!-- /.middle -->
            <!-- begin MegaNavbar-->
            <div class="nav-wrap-holder">
                <div class="container" id="nav_wrapper">
                    <nav class="navbar navbar-static-top navbar-default no-border-radius xs-height100" id="main_navbar" role="navigation">
                        <div class="container-fluid">
                            <!-- Brand and toggle get grouped for better mobile display -->
                            <div class="navbar-header">
                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#MegaNavbarID">
                                    <span class="sr-only">Toggle navigation</span>
                                    <span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span>
                                </button>
                                <a href="index.html" class="navbar-brand logo col-sm-3 visible-xs-block">
                                    <img src="images/logo-dark.png" class="img-responsive" alt="" />
                                </a>
                            </div>
                            <!-- Collect the nav links, forms, and other content for toggling -->
                            <div class="collapse navbar-collapse" id="MegaNavbarID">
                                <!-- regular link -->
                                <ul class="nav navbar-nav navbar-left col-md-12">
                                    <li class="dropdown-short " >
                                        <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-bars"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Saç Dökülme Tedavileri</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu no-border-radius no-shadow">
                                            <li class="divider no-margin"></li>

                                            <li class="no-margin divider"></li>
                                            @foreach($menu['tedavi'] as $td)
                                                <li><a href="{{url('sac-dokulme-tedavi/'.$td->path)}}">{{$td->category}}<span class="desc">{{$td->category_desc}}</span></a>
                                                </li>
                                            @endforeach

                                            <li class="divider"></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-short " >
                                        <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-bars"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Saç Ekimi</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu no-border-radius no-shadow">
                                            <li class="divider no-margin"></li>

                                            <li class="no-margin divider"></li>
                                            @foreach($menu['sac_ekimi'] as $sa)
                                                <li><a href="{{url('sac-ekimi/'.$sa->path)}}">{{$sa->category}}<span class="desc">{{$sa->category_desc}}</span></a>
                                                </li>
                                            @endforeach
                                            <li class="divider"></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-short ">
                                        <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-bars"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Sakal, Bıyık Ekimi</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu no-border-radius no-shadow">
                                            <li class="divider no-margin"></li>

                                            <li class="no-margin divider"></li>
                                            @foreach($menu['sakal_biyik'] as $td)
                                                <li><a href="{{url('sakal-biyik/'.$td->path)}}">{{$td->category}}<span class="desc">{{$td->category_desc}}</span></a>
                                                </li>
                                            @endforeach

                                            <li class="divider"></li>
                                        </ul>
                                    </li>
                                    <li class="dropdown-short ">
                                        <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-bars"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Kaş Ekimi</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu no-border-radius no-shadow">
                                            <li class="divider no-margin"></li>

                                            <li class="no-margin divider"></li>
                                            @foreach($menu['kas'] as $td)
                                                <li><a href="{{url('kas-ekimi/'.$td->path)}}">{{$td->category}}<span class="desc">{{$td->category_desc}}</span></a>
                                                </li>
                                            @endforeach

                                            <li class="divider"></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-short">
                                        <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-bars"></i>&nbsp;<span class="hidden-sm hidden-md reverse">Saç Dökülme Tipleri</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu no-border-radius no-shadow">
                                            <li class="divider no-margin"></li>

                                            <li class="no-margin divider"></li>
                                            <li><a href="">Nedenleri ve Tedavi Yöntemleri</a></li>
                                            <li><a href="">Lokal Saç Dökülmesi</a></li>
                                            <li><a href="">Erkek Tipi </a></li>
                                            <li><a href="">Kadın Tipi </a></li>

                                            <li class="divider"></li>
                                        </ul>
                                    </li>

                                    <li class="dropdown-full">
                                        <a data-toggle="dropdown" href="javascript:void(0);" class="dropdown-toggle"><i class="fa fa-envelope"></i> Bize Ulaşın &nbsp; <span class="hidden-sm hidden-md hidden-lg reverse">Account</span><span class="caret"></span></a>
                                        <ul class="dropdown-menu no-shadow no-border-radius space-xs">
                                            <li class="col-sm-4">
                                                <address>
                                                    <br>
                                                    <strong>İstanbul Saç Nakli Merkezi</strong><br>
                                                    Yeni Sülün Sokak<br>
                                                    No:13 3.Levent/İstanbul<br>
                                                    <abbr title="Phone">Mobil:</abbr> +90(532)543 0 300<br>
                                                    <abbr title="Mobile Phone">Mobil 2:</abbr> +90(532) 543 0 500
                                                </address>
                                                <address>
                                                    <br>
                                                    <a href="mailto:#">info@istanbulsacnakli.com</a>
                                                </address>

                                            </li>
                                            <li class="col-sm-7 col-sm-offset-1">
                                                <div id="message"></div>
                                                <form method="post" action="{{route('messageSend')}}" id="msgForm">
                                                    <fieldset>
                                                        <input type="hidden" id="token" value="{{csrf_token()}}">
                                                        <input class="form-control" name="title" type="text" id="name" size="30" value="" placeholder="Name" />

                                                        <br />
                                                        <input class="form-control" name="email" type="text" id="email" size="30" value="" placeholder="E-mail" />
                                                        <br />
                                                        <input class="form-control" name="phone" type="text" id="phone" size="30" value="" placeholder="Phone" />
                                                        <br />

                                                        <br />
                                                        <textarea class="form-control" name="message" cols="40" rows="3" id="comments" style="width: 100%;" placeholder="Your comments"></textarea>

                                                        <br />
                                                        <br />
                                                        <input type="submit" class="btn btn-primary btn-fill" id="submit" value="Gönder" />
                                                    </fieldset>
                                                </form>
                                            </li>
                                        </ul>
                                    </li>




                                </ul>

                            </div>
                        </div>
                    </nav>
                </div>
            </div>
            <!-- /.div nav wrap holder -->
        </header>
        <!-- end MegaNavbar-->
        @yield('content')



        <div class="footer-4cln space-md">
            <div class="container">
                <div class="row">
                    <div class="col-md-8">
                        <div class="row">
                            <div class="col-md-3 col-xs-6">
                                <h6>Company</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">About</a>
                                    </li>
                                    <li><a href="#">Meet the team</a>
                                    </li>
                                    <li><a href="http://sac-ektirmekistiyorum.blogspot.com">Blog</a>
                                    </li>
                                    <li><a href="#">Contact Us</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <h6>Account</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">Payments</a>
                                    </li>
                                    <li><a href="#">Subscriptions</a>
                                    </li>
                                    <li><a href="#">Gift Card</a>
                                    </li>
                                    <li><a href="#">Support</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <h6>Help</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">Payments</a>
                                    </li>
                                    <li><a href="#">Subscriptions</a>
                                    </li>
                                    <li><a href="#">Gift Card</a>
                                    </li>
                                    <li><a href="#">Support</a>
                                    </li>
                                </ul>
                            </div>
                            <div class="col-md-3 col-xs-6">
                                <h6>Legal</h6>
                                <ul class="list-unstyled">
                                    <li><a href="#">Payments</a>
                                    </li>
                                    <li><a href="#">Subscriptions</a>
                                    </li>
                                    <li><a href="#">Gift Card</a>
                                    </li>
                                    <li><a href="#">Support</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <h6>Subscribe to our newsletter</h6>
                        <form class="form-inline" role="form">
                            <div class="input-group">
                                <input type="email" name="subscribe" class="form-control footernews" placeholder="Enter Email">
                                <span class="input-group-btn">
                           <button class="btn btn-info" type="button" id="subscribe">Subscribe</button>
                           </span>
                            </div>
                            <span class="help-block">We'll never share your address.</span>
                        </form>
                        <div class="ourTeam-social">
                            <ul class="social">
                                <li><a href="#" class="social-btn social-facebook" title="Facebook"><i class="fa fa-facebook"></i></a>
                                </li>
                                <li><a href="#" class="social-btn social-twitter" title="Twitter"><i class="fa fa-twitter"></i></a>
                                </li>
                                <li><a href="#" class="social-btn social-google" title="Google+"><i class="fa fa-google-plus"></i></a>
                                </li>
                                <li><a href="#" class="social-btn social-linkedin" title="LinkedIn"><i class="fa fa-linkedin"></i></a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
                <hr>
                <div class="footer-4cln-bar" >
                    <span class="phone"><i class="fa fa-phone"></i> +90 532 543 0 300 - +90 532 543 0 500</span>
                    <span class="email"><i class="fa fa-envelope"></i> <a href="mailto:info@email.com">info@istanbulsacnakli.com</a></span>
                    <span class="address">Copyright &copy; İstanbul Saç Nakli,2017<br/> </span>
                </div>
            </div>
        </div>
    </div>
    <!-- end: animatsion -->
</div>
<!-- end:Wrapper -->
<!-- core JavaScript
     ================================================== -->
<script src="js/jquery.min.js"></script>
<script src="js/sweetalert.min.js"></script>
<script src="js/bootstrap.min.js"></script>
<script src="js/owl.carousel.min.js"></script>
<script src="js/plugins.js"></script>

@yield('scripts')



<!-- jQuery REVOLUTION Slider  -->
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.tools.min.js"></script>
<script type="text/javascript" src="rs-plugin/js/jquery.themepunch.revolution.min.js"></script>
<script src="js/custom.js"></script>
<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="js/ie10-viewport-bug-workaround.js"></script>

<script>
    $('#picker').datepicker({
        format: 'dd-mm-yy'
    });
    $('#submit').click(function(e){
        e.preventDefault()
        var link = $('#msgForm').attr('action')
        var arr= {
            '_token':$('#token').val(),
            'title': $('input[name="title"]').val(),
            'email': $('input[name="email"]').val(),
            'phone': $('input[name="phone"]').val(),
            'message': $('textarea[name="message"]').val()
        }
        $.post(link,arr,function(res){
            if(res){
                swal("Mesaj Gönderildi", "Mesajınız Başarıyla Gönderilmiştir. En Kısa Sürede Size Dönüş Sağlayacağız", "success")
            }
        })

    })
    $('#appoint').click(function(e){
        e.preventDefault();
        var arr={
            '_token':$('#token').val(),
            'name':$('input[name="appointname"]').val(),
            'surname':$('input[name="appointsurname"]').val(),
            'email':$('input[name="appointemail"]').val(),
            'date':$('#picker').val(),
            'phone':$('input[name="appointphone"]').val()

        }
        $.post('appointment',arr,function(res){
            if(res==='true'){
                swal('Randevu İsteği Gönderildi','Randevu İsteğiniz İletilmiştir','success')
            }

        }).fail(function(){
            $('#error').html('<li class="text-danger">Lütfen bütün alanları doğru bir şekilde doldurunuz</li>')
        })

    })

    $('#subscribe').click(function(){
        $.post('subscribe',{
            '_token':$('#token').val(),
            'email':$('input[name="subscribe"]').val()
        },function(res){
            if(res){
                swal('Mail Eklendi','Mail Adresiniz Subscriber Listesine Eklenmiştir','success')
            }
        })
    })


    $('#cd').datepicker({
        format: 'dd/mm/yyyy'
    });

</script>
</body>

</html>