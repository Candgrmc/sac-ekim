@extends('layouts.page')

@section('content')

    @if(isset($hizmet))
        <div id="service-sidebar">
            <div id="masthead" class="bg-image" data-image-src="{{asset('images/panorama.jpg')}}">
                <div class="container">
                    <div class="row">
                        <div class="col-md-6">
                            <ol class="breadcrumb" >
                                <li><a href="{{url('/')}}" style="color:#b9b9b9">Anasayfa</a>
                                </li>
                                <li><a href="{{url($hizmet->category.'/'.$sub->path)}}" style="color:#fff">{{ucfirst($sub->path)}}</a>
                                </li>
                            </ol>
                        </div>
                    </div>
                </div>
                <!--/container-->
            </div>
            <div class="container space-sm">
                <div class="col-sm-3">
                    <div class="book-box row">
                        <div class="book-form">
                            <h4 class="hr-after">Randevu Alın</h4>
                            <form action="{{route('appointment')}}" method="post">
                                {{csrf_field()}}
                                <div class="row">
                                    <p class="col-md-6 col-sm-6">
                                        <input class="form-control" type="text" placeholder="İsim" required="" name="name">
                                    </p>
                                    <p class="col-md-6 col-sm-6">
                                        <input class="form-control" type="text" placeholder="Soyisim" required="" name="surname">
                                    </p>
                                </div>
                                <p>
                                    <input class="form-control" type="email" placeholder="E-mail" required="" name="email">
                                </p>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="input-group date">
                                            <input type="text" class="form-control" value="16-10-2017" id="cd" name="date">
                                            <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <p>
                                            <input type="text" class="form-control" name="phone" placeholder="Telefon">
                                        </p>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <input type="hidden" value="">
                                </div>
                                <div class="row">

                                    <p class="col-md-6 col-sm-6">
                                        <button type="submit"  class="btn btn-primary btn-fill btn-block">Gönder</button>
                                    </p>
                                </div>
                            </form>
                            <hr style="border:1px solid #fff">
                            <p class="help-block">Your details will not be published. Before sumbition please read our <a href="#">Terms of service</a> and <a href="#">Privacy Policy</a>.</p>
                        </div>
                    </div>
                    <!-- /.form -->
                    <div class="spacer30"></div>

                    <!-- / nav block -->
                </div>
                <!-- /.col 4 -->
                <div class="col-sm-9 content-left">

                    <div id="myTabContent" class="tab-content no-border">
                        <div role="tabpanel" class="tab-pane fade active in" id="home" aria-labelledby="home">
                            <div class="row space-xs">
                                <div class="col-md-8">
                                    <h5>{{$sub->category}}</h5>
                                    <h3 class="hr-after"><strong>{{$sub->category_desc}}</strong>
                                        <br>.
                                    </h3>

                                    <p>
                                        {{$hizmet->aciklama}}
                                    </p>
                                    <!-- FEATURE LIST -->
                                    <div class="row">
                                        <div class="col-md-6 col-sm-6">
                                            <ul class="bullet-list list-unstyled">
                                                <li>Süre: {{$hizmet->sure}}</li>
                                                <li>Seans: {{$hizmet->seans_sayi}}</li>
                                            </ul>
                                        </div>
                                        <!-- <div class="col-md-6 col-sm-6">
                                            <ul class="bullet-list list-unstyled">
                                                <li>Office-Based Physicians</li>
                                                <li>Pharmacists and Pharmacies</li>
                                            </ul>
                                        </div>-->
                                    </div>
                                </div>
                                <!-- .col 7 -->
                                <div class="col-sm-4">
                                    <figure>
                                        <img src="http://placehold.it/400x600" alt="" class="img-responsive">
                                    </figure>
                                </div>
                                <!-- .col 3 -->
                            </div>
                        </div>

                    <!-- end tabs -->
              <div class="spacer50"></div>
                    <!-- start Faq section -->

                    <!-- end:Faq section -->
                </div>
                <!-- /.col 8 -->
            </div>
        </div>
        <div class="cta bg-blue-light">
            <div class="container">
                <div class="row cta-1">
                    <div class="cta-features">
                        <div class="col-sm-3 blue-1"><img src="{{asset('images\icons\airplane.png')}}" alt="" class="col-md-offset-1"><p  class="col-md-offset-1"><strong style="font-size:18px">Transfer
                                    <br><sub>Havaalanı-Otel-Hastane</sub></strong></p>
                        </div>
                        <div class="col-sm-3 blue-2"><img src="{{asset('images\icons\bunk.png')}}" alt="" class="col-md-offset-1"><p  class="col-md-offset-1"><strong style="font-size:18px">Konaklama
                                    <br><sub>Otel Konaklama Masrafları</sub></strong></p>
                        </div>
                        <div class="col-sm-3 blue-3"><img src="{{asset('images\icons\epidermis.png')}}" alt="" class="col-md-offset-1"><p  class="col-md-offset-1"><strong style="font-size:18px"><span class="">ÜCRETSİZ</span>
                                    <br><sub>Muayene ve Saç Analizi</sub></strong></p>
                        </div>
                        <div class="col-sm-3 blue-4"><img src="{{asset('images\icons\crown.png')}}" alt="" class="col-md-offset-1"><strong style="font-size:18px">İSTANBUL SAÇ NAKLİ<sub>ile ayrıcalıklısınız</sub></strong>
                        </div>
                    </div>
                </div>
            </div>
        </div>


    @endif

@endsection