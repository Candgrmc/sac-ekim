@extends('layouts.page')

@section('content')
    <div id="service-sidebar">
        <div id="masthead" class="bg-image" data-image-src="{{asset('images/panorama.jpg')}}">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <ol class="breadcrumb">
                            <li ><a style="color:#b9b9b9" href="{{url('/')}}">Anasayfa</a>
                            </li>
                            <li ><a style="color:#fff" href="{{url('/hakkimizda')}}">Hakkımızda</a>
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
            <!--/container-->
        </div>
        <div class="container space-sm">
            <div class="col-sm-3">
                <div class="book-box row">
                    <div class="book-form">
                        <h4 class="hr-after">Randevu Alın</h4>
                        <form action="{{route('appointment')}}" method="post">
                            {{csrf_field()}}
                            <div class="row">
                                <p class="col-md-6 col-sm-6">
                                    <input class="form-control" type="text" placeholder="İsim" required="" name="name">
                                </p>
                                <p class="col-md-6 col-sm-6">
                                    <input class="form-control" type="text" placeholder="Soyisim" required="" name="surname">
                                </p>
                            </div>
                            <p>
                                <input class="form-control" type="email" placeholder="E-mail" required="" name="email">
                            </p>
                            <div class="row">
                                <div class="col-md-12">
                                    <div class="input-group date">
                                        <input type="text" class="form-control" value="16-10-2017" id="cd" name="date">
                                        <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                    </div>

                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-12">
                                    <p>
                                        <input type="text" class="form-control" name="phone" placeholder="Telefon">
                                    </p>
                                </div>
                            </div>
                            <div class="form-group">
                                <input type="hidden" value="">
                            </div>
                            <div class="row">

                                <p class="col-md-6 col-sm-6">
                                    <button type="submit"  class="btn btn-primary btn-fill btn-block">Gönder</button>
                                </p>
                            </div>
                        </form>
                        <hr style="border:1px solid #fff">
                        <p class="help-block">Your details will not be published. Before sumbition please read our <a href="#">Terms of service</a> and <a href="#">Privacy Policy</a>.</p>
                    </div>
                </div>
                <!-- /.form -->
                <div class="spacer30"></div>

                <!-- / nav block -->
            </div>
            <!-- /.col 4 -->
            <div class="col-sm-9 content-left">
                <div class="row">
                    <div class="col-sm-12">
                        <div id="owl-slider-2" class="owl owl-carousel owl-theme">
                            <div class="item">
                                <img src="images/hastane.jpg" alt="The Last of us">
                            </div>
                        </div>
                        <!-- /#owl slider -->
                    </div>
                    <!-- /col - sm - 12 -->
                </div>
                <div class="row space-xs">
                    <div class="col-sm-6">
                        <h3>Hakkımızda</h3>

                        <p>
                            Günümüzde genç kalmanın, bakımlı ve güzel olmanın,
                            prezentabl görünmenin ne kadar önemli olduğunu görüyoruz ve biliyoruz.
                            ,İnsanların bu kadar önemsediği bir konuda kaliteli bir hizmet vermek için ekibimiz sac ekim operasyonlarını birinci sınıf hastanelerde ameliyathane şartlarında, tüm dünyada kabul görmüş ve uygulanmakta olan FUE yöntemiyle gercekleştirmektedir. 15 yılı aşkın bir süredir saç ekim hizmeti veren ekibimiz, konusunda uzman doktor ve hemşirelerden oluşur. Saç Ekim Uzmanlarımızın tıbbi alt yapısı, tecrübesi, deneyimi ve en son çıkan cihaz ve ekipmanlarla bu işi yapıyor olması ve tüm dünyadaki yenilikleri yakından takip ediyor olması bizim size sunduğumuz hizmetin kalitesini arttırmaktadır. Ekibimizin kaliteli hizmet politikası bununla da sınırlı değil. Kişilerin konuyla ilgili bilgilendirilmesi, konsültasyonu, saç ekim operasyonu sonrasında bakım ve takiplerinin yanısıra saç sağlığı konusunda da her türlü danışma hizmetini de vermektir.
                        </p>
                       


                    </div>
                    <!-- /.col 6 -->
                    <span class="ver-sep"></span>
                    <div class="col-sm-6">
                        <h3>Uzmanlıklarımız</h3>
                        <p class="lead">You can find general information about making appointments, as well as other helpful tips.</p>
                        <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Ut volutpat rutrum eros sit amet sollicitudin. Suspendisse pulvinar,</p>
                        <div class="spacer30"></div>
                        <div class="panel-group" id="accordion">
                            <div class="panel">
                                <div class="panel-heading blue-border">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                            Bize Nasıl Ulaşırsınız
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseOne" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <strong>Telefon:</strong> +90 (532) 543 0 300- +90 (532) 543 0 500
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading light-blue-border">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                            Operasyon Öncesi
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseTwo" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        Nihil anim keffiyeh helvetica, craft beer labore wes anderson cred nesciunt sapiente ea proident. Ad vegan excepteur butcher vice lomo. Leggings occaecat craft beer farm-to-table, raw denim aesthetic synth nesciunt you probably haven't heard of them accusamus labore sustainable VHS.
                                    </div>
                                </div>
                            </div>
                            <div class="panel">
                                <div class="panel-heading red-border">
                                    <h4 class="panel-title">
                                        <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion" href="#collapseThree">
                                            Operasyon Sonrası
                                            <span class="glyphicon glyphicon-plus"></span>
                                        </a>
                                    </h4>
                                </div>
                                <div id="collapseThree" class="panel-collapse collapse">
                                    <div class="panel-body">
                                        <ul class="bullet-list list-unstyled">
                                            <li>Contact Office-Based Physicians</li>
                                            <li>Target Pharmacists and Pharmacies</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>

                        </div>
                    </div>
                    <!-- /.col 6 -->
                </div>
                <hr>

            </div>
            <!-- /.col 9 -->
        </div>
    </div>

@endsection