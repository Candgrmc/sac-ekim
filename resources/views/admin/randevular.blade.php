@extends('admin.layout.panel')

@section('content')
    <div class="col-md-9">
        <table class="table table-striped">
            <thead>
            <tr>
                <th>İsim</th>
                <th>Soyisim</th>
                <th>Telefon</th>
                <th>Email</th>
                <th>Tarih</th>
            </tr>
            </thead>
            <tbody>
            @foreach($randevu as $rd)
            <tr>
                <td>{{$rd->name}}</td>
                <td>{{$rd->surname}}</td>
                <td>{{$rd->phone}}</td>
                <td>{{$rd->email}}</td>
                <td>{{$rd->date}}</td>
            </tr>
            @endforeach
            </tbody>

        </table>
    </div>

@endsection