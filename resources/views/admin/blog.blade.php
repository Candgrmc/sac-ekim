@extends('admin.layout.panel')

@section('content')
    <div class="col-md-9">
        <form action="">
            <label for="title">Başlık</label>
            <input type="text" class="form-control" name="title">

            <label for="text">Makale</label>
            <textarea name="text" id="" cols="30" rows="10" class="form-control"></textarea>

            <label for="keywords">Keywords</label>
            <input type="text" class="form-control" name="keywords">

            <label for="img">Resim</label>
            <input type="file" name="img" class="form-control">
            <button class="btn btn-primary btn-fill btn-wide" style="margin-top:20px"><i class="fa fa-save"></i> Gönder</button>
        </form>
    </div>
    
    
@endsection    