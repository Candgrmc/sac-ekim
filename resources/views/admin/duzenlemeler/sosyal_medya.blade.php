@extends('admin.layout.panel')

@section('content')



    <form action="{{route('socialpost')}}" method="POST" class="col-md-12">

        {!! csrf_field() !!}

        <div class="col-md-9">
            <label for="facebook"><i class="fa fa-facebook"></i> Facebook</label>
            <input type="text" class="form-control" name="facebook" value="{{$social->facebook}}">

    <label for="twitter"><i class="fa fa-twitter"></i> Twitter</label>
    <input type="text" class="form-control" name="twitter" value="{{$social->twitter}}">


    <label for="instagram"><i class="fa fa-instagram"></i> Instagram</label>
    <input type="text" class="form-control" name="instagram" value="{{$social->instagram}}">

        </div>


            <div class="col-md-12" style="margin-top: 20px;">
                <button class="btn btn-fill btn-primary"><i class="fa fa-check-circle-o" type="submit"></i>
                    Kaydet
                </button>
            </div>

    </form>

@endsection