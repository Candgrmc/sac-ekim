@extends('admin.layout.panel')

@section('content')
    <div class="col-md-9 well">
        <h3><i class="fa fa-pagelines"></i> Hizmet Açıklaması Ekleyiniz</h3>

        @if(session('status'))

                <div class="alert alert-success"><i class="fa fa-check-circle-o"></i> {{session('status')}}</div>


        @endif
        <form action="{{route('sacekimpost')}}" method="POST">
            {!! csrf_field() !!}
            <label for="aciklama">Açıklama</label>
            <textarea class="form-control" name="aciklama" cols="10" rows="10"></textarea>

            <label for="kimler_icin">Kimler İçin Kullanılır?</label>
            <textarea class="form-control" name="kimler_icin" cols="10" rows="10"></textarea>


            <label for="sure">Süre:</label>
            <input type="text" class="form-control"  name="sure">

            <label for="seans_sayi">Seans Sayısı:</label>
            <input type="text" class="form-control"  name="seans_sayi">

            <label for="servis_sonrası">Seans Sayısı:</label>
            <textarea class="form-control"  name="servis_sonrası" cols="10" rows="10"></textarea>

            <label for="keywords">Anahtar Kelimeler</label>
            <input type="text" class="form-control" name="keywords">

            <label for="img">Resim</label>

            <label for="sub_category">Sub Kategori</label>
            <select name="sub_category" class="form-control">
                @foreach($sub as $sb)
                    <option value="{{$sb->id}}">{{$sb->category}}</option>
                @endforeach
            </select>

            <div class="col-md-6 col-md-offset-3" style="margin-top:20px">
                <button class="btn btn-fill btn-primary col-md-12" >Ekle</button>
            </div>


        </form>
    </div>
    <div class="well col-md-3">
        <h3><i class="fa fa-plus"></i> Saç Ekimi Kategori Ekle</h3>
        <form action="{{route('sacekimcat')}}" method="POST">
            {!! csrf_field() !!}
            <label for="category">Kategori Adı</label>
            <input type="text" class="form-control" name="category">
            <label for="category">Kategori Açıklaması</label>
            <input type="text" class="form-control" name="category_desc">
            <label for="category">Kategori Yolu</label>
            <input type="text" class="form-control" name="path">

            <center><button class="btn btn-success " style="margin-top:15px; width: 100%">Ekle</button></center>
        </form>
    </div>
@endsection