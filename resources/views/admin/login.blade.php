@extends('layouts.page')

@section('content')

    <div class="well col-md-6 col-md-offset-3" style="margin-top:20px">
        <form action="{{url('logmein')}}" method="POST">
            {!! csrf_field() !!}
            <label for="email">E-mail</label>
            <input type="text" class="form-control" name="email">
            <label for="password">Şifre</label>
            <input type="password" class="form-control" name="password">
            <div class="col-md-6 col-md-offset-3" style="margin-top:15px">
                <button class="btn btn-wide btn-default align-center col-md-12" type="submit">Giriş</button>
            </div>
        </form>

    </div>

@endsection