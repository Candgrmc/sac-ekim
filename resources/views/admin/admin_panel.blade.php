@extends('admin.layout.panel')

@section('content')

<div class="container-fluid" style="background-color: #fff;padding:30px;border-radius:10px">
    <div class="col-md-3 col-md-offset-2 col-xs-5 col-xs-offset-1" style="padding:10px;background-color: #bd5151;color:#fff;height: 100px;border-radius:10px">
        <center>Randevular</center>
        <span style="position: absolute;top:40%;left:40%;margin:0 auto; font-size: 50px">
        {{$sayi['randevu']}}
    </span>
    </div>
    <div class="col-md-3 col-md-offset-1 col-xs-5 col-xs-offset-1" style="padding:10px;background-color: #bd5151;color:#fff;height: 100px;border-radius:10px">
        <center>Mesajlar</center>
        <span style="position: absolute;top:40%;left:40%;margin:0 auto; font-size: 50px">
        {{App\message::where('status',0)->get()->count()}}
    </span>
    </div>
</div>


@endsection

