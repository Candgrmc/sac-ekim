@extends('layouts.page')

@section('content')
    <!-- start: Slider -->
    <div class="fullsize-container">
        <div class="fullsize-slider">
            <ul>
                <li data-transition="fade" data-slotamount="1" data-thumb="http://placehold.it/200x200" data-delay="7500" data-saveperformance="off" data-title="Our Workplace">
                    <!-- MAIN IMAGE -->
                    <img src="images/sacekimi.png" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">
                    <!-- LAYERS -->
                    <div class="tp-caption theme-caption rs-parallaxlevel-4 transform " data-x="20" data-y="center" data-speed="700" data-voffset="-75" data-start="600" data-easing="Power3.easeInOut">
                        <strong><br>Mevsim Geçişlerinde <br>Saç Mezoterapisi</strong>

                    </div>
                    <div class="tp-caption sfl rs-parallaxlevel-4" data-x="20" data-y="center" data-voffset="15" data-speed="1000" data-start="2000" data-easing=""><a href="#service" class="btn-round tp-simpleresponsive button blue"></a>
                    </div>
                </li>
                <!-- SLIDE  -->
                <li data-transition="fade" data-slotamount="1" data-masterspeed="1500" data-thumb="http://placehold.it/200x200" data-delay="7000" data-saveperformance="off" data-title="Slide">
                    <!-- MAIN IMAGE -->
                    <img src="images/sacekim1.jpg" alt="fullslide6" data-bgposition="center top" data-bgfit="cover" data-bgrepeat="no-repeat">
                    <!-- LAYERS -->
                    <div class="caption theme-caption rs-parallaxlevel-4 transform " data-x="20" data-y="center" data-speed="700" data-voffset="-95" data-start="600" data-easing="Power3.easeInOut">
                        <strong><br>Saç Ekimi İçin Uygun
                            <br>Bir Aday Mısınız?</strong>
                    </div>
                    <div class="caption mediumlarge_light_dark rs-parallaxlevel-4" data-x="20" data-y="center" data-speed="800" data-voffset="-20" data-start="1500" data-easing="Power3.easeInOut"></div>
                    <div class="caption sfl rs-parallaxlevel-4" data-x="20" data-y="center" data-voffset="27" data-speed="1000" data-start="2000" data-easing="">
                    </div>
                </li>

                <li data-transition="fade" data-slotamount="1" data-thumb="http://placehold.it/200x200" data-delay="7500" data-saveperformance="off" data-title="Our Workplace">
                    <!-- MAIN IMAGE -->
                    <img src="images/maxresdefault.jpg" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">
                    <!-- LAYERS -->
                    <div class="tp-caption theme-caption rs-parallaxlevel-4 transform " data-x="20" data-y="center" data-speed="700" data-voffset="-75" data-start="600" data-easing="Power3.easeInOut">
                       <strong> <br>Yüzünüze Uygun <br>Mükemmel Kaşlar</strong>

                    </div>
                    <div class="tp-caption sfl rs-parallaxlevel-4" data-x="20" data-y="center" data-voffset="15" data-speed="1000" data-start="2000" data-easing=""><a href="#service" class="btn-round tp-simpleresponsive button blue"></a>
                    </div>
                </li>
                <li data-transition="fade" data-slotamount="1" data-thumb="http://placehold.it/200x200" data-delay="7500" data-saveperformance="off" data-title="Our Workplace">
                    <!-- MAIN IMAGE -->
                    <img src="images/sacekim2.jpg" data-bgfit="cover" data-bgrepeat="no-repeat" alt="">
                    <!-- LAYERS -->
                    <div class="tp-caption theme-caption rs-parallaxlevel-4 transform " data-x="20" data-y="center" data-speed="700" data-voffset="-75" data-start="600" data-easing="Power3.easeInOut">
                        <strong><br>Saç Dökülmesinde <br>Bilinmeyen Gerçekler</strong>

                    </div>
                    <div class="tp-caption sfl rs-parallaxlevel-4" data-x="20" data-y="center" data-voffset="15" data-speed="1000" data-start="2000" data-easing=""><a href="#service" class="btn-round tp-simpleresponsive button blue"></a>
                    </div>
                </li>
            </ul>
        </div>
    </div>
    <!-- end: Slider -->
    <div class="horizontal-form med-tab">
        <div class="container">
            <div class="tabbable-panel">
                <div class="tabbable-line">
                    <ul class="nav nav-tabs">
                        <li class="active">
                            <a data-toggle="tab" href="#book">Ücretsiz Muayene Olun</a>
                        </li>
                        <li class="">
                            <a data-toggle="tab" href="#info">Bize Ulaşın </a>
                        </li>
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="book">
                            <form class="space-xs">
                                <p id="error"></p>
                                <div class="row">
                                    <div class="col-sm-8">
                                        <div class="row">
                                            <p class="col-md-6 col-sm-6">
                                                <input class="form-control" placeholder="İsim" type="text" name="appointname">
                                            </p>
                                            <p class="col-md-6 col-sm-6">
                                                <input class="form-control" placeholder="Soyisim" type="text" name="appointsurname">
                                            </p>
                                        </div>
                                    </div>
                                    <!-- /.col 8 -->
                                    <div class="col-sm-4">
                                        <input class="form-control sm-margin-bottom-10" placeholder="Email" required="" type="email" name="appointemail">
                                    </div>
                                    <!-- /.col 4 -->
                                </div>
                                <!-- /row -->
                                <div class="row">
                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group date sm-margin-bottom-10">
                                                    <input class="form-control"  placeholder="Randevu Tarihi" id="picker" type="text"> <span class="input-group-addon"><span class="glyphicon glyphicon-calendar"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="col-sm-4">
                                        <div class="row">
                                            <div class="col-md-12">
                                                <div class="input-group date sm-margin-bottom-10">
                                                    <input class="form-control"  placeholder="Telefon"  type="text" name="appointphone"> <span class="input-group-addon"><span class="glyphicon glyphicon-phone"></span></span>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <!-- /.col 4 -->

                                    <!-- /.col 3 -->
                                    <div class="col-sm-2">
                                        <a class="btn btn-default btn-block" href="#" id="appoint">Gönder</a>
                                    </div>
                                    <!-- /.col 1 -->
                                </div>
                                <!-- /.row -->
                            </form>
                        </div>
                        <!-- /.tab pane -->
                        <div class="tab-pane fade" id="info">
                            <div class="spacer40"></div>
                            <div class="col-sm-4">
                                <div class="med-iconBox med-iconBox--left">
                                    <div class="med-iconBox-icon icon-big color-blue">
                                        <span class="icon-i-dental" aria-hidden="true"></span>
                                    </div>
                                    <div class="med-iconBox-content">
                                        <h4 class="med-iconBox-title hr-after">
                                            0080 123 456874
                                        </h4>
                                        <p>
                                            For dental emergency please call
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col 4 -->
                            <div class="col-sm-4">
                                <div class="med-iconBox med-iconBox--left">
                                    <div class="med-iconBox-icon icon-big color-red">
                                        <span class="icon-i-emergency" aria-hidden="true"></span>
                                    </div>
                                    <div class="med-iconBox-content">
                                        <h4 class="med-iconBox-title hr-after">
                                            0080 123 456874
                                        </h4>
                                        <p>
                                            For global case plase call
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col 4 -->
                            <div class="col-sm-4">
                                <div class="med-iconBox med-iconBox--left">
                                    <div class="med-iconBox-icon icon-big color-green-light">
                                        <span class="icon-i-first-aid" aria-hidden="true"></span>
                                    </div>
                                    <div class="med-iconBox-content">
                                        <h4 class="med-iconBox-title hr-after">
                                            0080 123 4568724
                                        </h4>
                                        <p>
                                            For online assitance plase call
                                        </p>
                                    </div>
                                </div>
                            </div>
                            <!-- /.col 4 -->
                        </div>
                        <!-- /.tab pane -->
                    </div>
                    <!-- /.tab content -->
                </div>
                <!-- /.tabbable line-->
            </div>
            <!-- /.tabbable panel -->
        </div>
        <!-- /.container -->
    </div>


    <hr/>
    <section class="space-md">
        <div class="container">
            <div class="row">
                <div class="col-sm-3">
                    <p class="lead hr-after">Perfect for your business</p>
                    <p>Phasellus enim libero,ut et lobortis aliquam aliquam in tortor et libero, blandit vel sapi condimentum ultricies magn</p>
                </div>
                <div class="col-sm-9">
                    <div class="row margin-bottom-35">
                        <div class="col-sm-6">
                            <div class="med-iconBox med-iconBox--left">
                                <div class="med-iconBox-icon icon-big color-blue">
                                    <span class="icon-i-alternative-complementary" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                </div>
                                <div class="med-iconBox-content">
                                    <h4 class="med-iconBox-title hr-after">
                                        Saç Ekimi Kimlere Uygulanır?
                                    </h4>
                                    <p>
                                        Sağlıklı her bireye saç problemi olduğu sürece saç ekimi uygulanır. Ancak daha öncesinde yani dökülmenin yeni başladığı evrelerde diğer tedavi yöntemlerini de denemekte fayda vardır. Artık mezoterapinin ya da botoks destekli mezoterapinin, bir takım saç dökülmesini durdurucu özelliği olan ilaçların kullanılmasından sonra hiç bir sonuç alınamıyorsa yine tek çözüm saç ekimidir.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /.med box -->
                        <div class="col-sm-6">
                            <div class="med-iconBox med-iconBox--left">
                                <div class="med-iconBox-icon icon-big color-blue">
                                    <span class="icon-i-laboratory" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                </div>
                                <div class="med-iconBox-content">
                                    <h4 class="med-iconBox-title hr-after">
                                        Saç Klonlama Nedir? Türkiye’de Uygulanıyor mu?
                                    </h4>
                                    <p>
                                        Şuanda tüm dünyada uygulanan en etkili ve en başarılı yöntem saç ekimidir. Yani ense ve kulak üstlerinden köklerin yoğun olduğu bölgeden kıl foliküllerinin tek tek çıkartılarak saçsız alanlara nakledilmesi işlemidir. Bu yönteme saç ekiminde Fue Tekniği deniyor. Fue Tekniğinde başarı %100, saçlar doğal ve dökülmeme özelliğine sahip, ayrıca herhangi bir reaksiyon riski yok. Çünkü kişinin kendi mevcut saç köklerinden alınarak yine kendisine ekiliyor.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /.med box -->
                    </div>
                    <div class="row margin-bottom-35">
                        <div class="col-sm-6">
                            <div class="med-iconBox med-iconBox--left">
                                <div class="med-iconBox-icon icon-big color-blue">
                                    <span class="icon-i-family-practice"></span>
                                </div>
                                <div class="med-iconBox-content">
                                    <h4 class="med-iconBox-title hr-after">
                                        Saç Ekim Operasyonları Neden Hastanelerde Uygulanmalıdır?
                                    </h4>
                                    <p>
                                        Saç ekim operasyonu basit bir işlemmiş gibi görünse de 4-5 saat süren ve ciddi bir estetik operasyonudur.  Bu nedenle steril koşullarda ve steril tıbbi malzemelerle çalışmak gereklidir. Bu da ancak ameliyathanelerde mümkündür. Çünkü hastanelerin ameliyathaneleri en steril ortamlardır. Saç ekim ameliyathaneleri, hastane içerisinde rahatça televizyonunuzu izleyip müziğinizi dinleyebileceğiniz çok konforlu ortamlardır.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /.med box -->
                        <div class="col-sm-6">
                            <div class="med-iconBox med-iconBox--left">
                                <div class="med-iconBox-icon icon-big color-blue">
                                    <span class="icon-i-information-us" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                </div>
                                <div class="med-iconBox-content">
                                    <h4 class="med-iconBox-title hr-after">
                                        Sakal,Favori,Bıyık Ekimi
                                    </h4>
                                    <p>
                                        Yüzde sakal, bıyık ve favori bölgesinde yanık, kesik, yara izi gibi kılın çıkmadığı durumlarda kaşta olduğu gibi yine vücuttan alınan kıl folikülleri, eksikliği görülen bu alanlara ekilebilir. Yaklaşık 2 saat süren bu operasyon lokal anestezi altında yapılır. Operasyon sonunda hasta kısa sürede eski görünümüne ağrı veya acı hissetmeden ulaşır. Ancak yüz bölgesinde yapılan kök nakillerinde en çok önemsenmesi gereken husus doğallıktır. Ekilecek alanda öncelikle yapılacak planlama, çizim, ekilen köklerin derinliği ve açısı işin püf noktasıdır. Bu sebeple iş yine uzman bir ekibe düşüyor.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /.med box -->
                    </div>
                    <div class="row">
                        <div class="col-sm-6">
                            <div class="med-iconBox med-iconBox--left">
                                <div class="med-iconBox-icon icon-big color-blue">
                                    <span class="icon-i-text-telephone" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                </div>
                                <div class="med-iconBox-content">
                                    <h4 class="med-iconBox-title hr-after">
                                        Konsultasyon
                                    </h4>
                                    <p>
                                        adsa
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /.med box -->
                        <div class="col-sm-6">
                            <div class="med-iconBox med-iconBox--left">
                                <div class="med-iconBox-icon icon-big color-blue">
                                    <span class="icon-i-coffee-shop" data-toggle="tooltip" data-placement="top" title="" data-original-title="Web Design"></span>
                                </div>
                                <div class="med-iconBox-content">
                                    <h4 class="med-iconBox-title hr-after">
                                        Operasyon Sonrası
                                    </h4>
                                    <p>
                                        Praesent faucibus nisl sit amet nulla sollicitudin pretium a sed purus. Nullam bibendum porta magna.
                                    </p>
                                </div>
                            </div>
                        </div>
                        <!-- /.med box -->
                    </div>
                </div>
            </div>
        </div>
    </section>
   <!-- <div class="benefits-quote bg-light">
        <div class="container-fluid tile-container">
            <div class="row">
                <div class="col-sm-12 col-md-6 bg-image tile-item" data-image-src="http://placehold.it/1600x800">
                </div>
                <div class="col-sm-12 col-md-6">
                    <div class="quote">
                        <p>After 8 Gold Medals</p>
                        <blockquote>You can't put a limit on anything. The more you dream, the farther you get.</blockquote>
                        <img src="http://placehold.it/150x150" alt="" class="img-responsive img-circle" />
                        <h2>Dummy Name</h2>
                        <h6>CEO at Dummy</h6>
                    </div>
                </div>
            </div>
        </div>
    </div> -->
    <!-- /.benefits -->


    <!-- /.tips and news -->
    <div class="cta bg-blue-light">
        <div class="container">
            <div class="row cta-1">
                <div class="cta-features">
                    <div class="col-sm-3 blue-1"><img src="{{asset('images\icons\airplane.png')}}" alt="" class="col-md-offset-1"><p  class="col-md-offset-1"><strong style="font-size:18px">Transfer
                                <br><sub>Havaalanı-Otel-Hastane</sub></strong></p>
                    </div>
                    <div class="col-sm-3 blue-2"><img src="{{asset('images\icons\bunk.png')}}" alt="" class="col-md-offset-1"><p  class="col-md-offset-1"><strong style="font-size:18px">Konaklama
                                <br><sub>Otel Konaklama Masrafları</sub></strong></p>
                    </div>
                    <div class="col-sm-3 blue-3"><img src="{{asset('images\icons\epidermis.png')}}" alt="" class="col-md-offset-1"><p  class="col-md-offset-1"><strong style="font-size:18px"><span class="">ÜCRETSİZ</span>
                                    <br><sub>Muayene ve Saç Analizi</sub></strong></p>
                    </div>
                    <div class="col-sm-3 blue-4"><img src="{{asset('images\icons\crown.png')}}" alt="" class="col-md-offset-1"><strong style="font-size:18px">İSTANBUL SAÇ NAKLİ<sub>ile ayrıcalıklısınız</sub></strong>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection
