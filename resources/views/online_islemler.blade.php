@extends('layouts.page')

@section('content')
    <script src="js/webcam.min.js"></script>
    <script>
        Webcam.set({
            width: 160,
            height: 120,
            image_format: 'jpeg',
            jpeg_quality: 90
        });
    </script>
    <div class="container col-md-10 col-md-offset-1 well" style="margin-top:30px; margin-bottom:100px;">
        <form action="" method="post" enctype="multipart/form-data">
            <div class="col-md-6">
                <label for="name_surname">İsim Soyisim</label>
                <input type="text" class="form-control" name="name_surname">
                <label for="mail">E-posta Adresiniz</label>
                <input type="email" class="form-control" name="mail">
                <label for="phone">Telefon Numaranız</label>
                <input type="text" class="form-control" name="phone">
                <label for="gender">Cinsiyetiniz</label>
                <select class="form-control" name="gender">
                    <option value="Bay">Bay</option>
                    <option value="Bayan">Bayan</option>
                </select>
                <label for="city">Yaşadığınız Şehir</label>
                <input type="text" class="form-control" name="city">

                <label for="date_of_birth">Doğum Tarihiniz</label>
                <input type="date" class="form-control" name="date_of_birth">


                <label for="sigara">Sigara Kullanıyor Musunuz?</label>
                <select name="sigara" id="" class="form-control">
                    <option value="Evet">Evet</option>
                    <option value="Hayır">Hayır</option>
                </select>
                <label for="alkol">Alkol Kullanıyor Musunuz?</label>
                <select name="alkol" id="" class="form-control">
                    <option value="Evet">Evet</option>
                    <option value="Hayır">Hayır</option>
                </select>
                <label for="ilac">Düzenli Olarak Herhangi Bir İlaç Kullanıyor Musunuz?</label>
                <select name="ilac" id="" class="form-control">
                    <option value="Evet">Evet</option>
                    <option value="Hayır">Hayır</option>
                </select>
            </div>

            <div class="col-md-6">


                <label for="alerji">Anesteziye ya da Herhangi Bir İlaca Alerjiniz Var Mı?</label>
                <select name="alerji" id="" class="form-control">
                    <option value="Evet">Evet</option>
                    <option value="Hayır">Hayır</option>
                </select>

                <label for="sac_dokulme">Ailenizde Saç Dökülmesi Var Mı?</label>
                <select name="sac_dokulme" id="" class="form-control">
                    <option value="Evet">Evet</option>
                    <option value="Hayır">Hayır</option>
                </select>

                <label for="sac_dokulme_sure">Saç Dökülme Süresi</label>
                <input type="text" class="form-control" name="sac_dokulme_sure">

                <label for="sac_dokulme_neden">Saç Dökülme Nedeni</label>
                <input type="text" class="form-control" name="sac_dokulme_neden">

                <label for="sistemik_hastalik">Sistemik Bir Hastalığınız Varsa Belirtiniz</label>
                <input type="text" class="form-control" name="sistemik_hastalik">

                <label for="cerrahi">Geçirilmiş Bir Cerrahiniz Varsa Belirtiniz</label>
                <input type="text" class="form-control" name="cerrahi">

                <div class="col-md-12">
                    <h4>3 Açıdan Fotoğrafınızı Yükleyiniz</h4>
                    <div class="col-md-4">
                        <label for="foto1">1.Foto</label>
                        <input type="file" class="form-control" name="foto1">
                    </div>
                    <div class="col-md-4">
                        <label for="foto2">2.Foto</label>
                        <input type="file" class="form-control" name="foto2">
                    </div>
                    <div class="col-md-4">
                        <label for="foto3">3.Foto</label>
                        <input type="file" class="form-control" name="foto3">
                    </div>
                </div>


                <div class="col-md-12" style="margin-top:30px">
                    <li class="text-danger">Fotoğraf çekme eklentisi ssl sertifikası yapılandırılmadığı için henüz çalışmamaktadır</li>
                    <div class="col-md-4" id="foto1">
                        <label for="">1.Foto</label>

                    </div>
                    <div class="col-md-4" id="foto2">
                        <label for="">2.Foto</label>

                    </div>
                    <div class="col-md-4" id="foto3">
                        <label for="">3.Foto</label>

                    </div>
                </div>



            </div>
            <div class="col-md-8 col-md-offset-2" style="margin-top:50px">
                <h4>Fotoğraf Çekmek İçin Kamera Eklentisine İzin Verin</h4>
                <div class="col-md-4">
                    <div id="webcam1">

                    </div>
                    <div class="hidden" id="buttons" style="display: inline-block">
                        <input type="button" value="Çek" class="btn btn-royal btn-fill " id="foto_cek" onclick="javascript:void(take_snapshot())">
                        <input type="button" class="btn btn-fill btn-danger" id="close_cam" onclick="javascript:void(closeCam())" value="Kapat">
                    </div>
                </div>
            </div>

            <div class="col-md-12">
                <button class="btn btn-primary btn-fill col-md-6 col-md-offset-3" style="margin-top:30px">Gönder</button>
            </div>


        </form>
        <button id="opencam" class="btn btn-fill btn-primary" onclick="javascript:void(setup())">Fotoğraf Çek</button>

    </div>

    <script>
        var i=1;
        function take_snapshot() {


            // take snapshot and get image data
            if(i<4){
                Webcam.snap( function(data_uri) {
                    // display results in page
                    document.getElementById('foto'+i).innerHTML ='<img src="'+data_uri+'"/>';
                } );
                i++;
                if(i==4){
                    $('#buttons').addClass('hidden')
                    Webcam.reset();
                }

            }
        }

        function setup(){
            Webcam.reset();
            Webcam.attach('#webcam1');
            $('#buttons').removeClass('hidden')
            $('#opencam').addClass('hidden')

        }

        function closeCam(){
            Webcam.reset()
            $('#buttons').addClass('hidden')
            $('#opencam').removeClass('hidden')

        }
    </script>

@endsection

@section('scripts')

    <script>



    </script>
@endsection



