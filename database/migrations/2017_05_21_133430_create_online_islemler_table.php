<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateOnlineIslemlerTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('online_islemler', function (Blueprint $table) {
            $table->increments('id');
            $table->string('name_surname');
            $table->string('mail');
            $table->string('phone');
            $table->string('gender');
            $table->string('city');
            $table->string('date_of_birth');
            $table->string('sigara');
            $table->string('alkol');
            $table->string('ilac');
            $table->string('alerji');
            $table->string('sac_dokulme');
            $table->string('sac_dokulme_sure');
            $table->string('sistemik_hastalik');
            $table->string('cerrahi');
            $table->string('foto1');
            $table->string('foto2');
            $table->string('foto3');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('online_islemler');
    }
}
