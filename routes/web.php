<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/','MainController@index');

Route::get('/{category}/{sub_category}','MainController@hizmet_detay');
Route::get('blog','MainController@blog');
Route::post('appointment','MainController@appointment')->name('appointment');
Route::post('message','MainController@messageSend')->name('messageSend');
Route::post('subscribe','MainController@subscribe');

Auth::routes();
Route::get('/hakkimizda','MainController@hakkimizda');
Route::get('/iletisim','MainController@iletisim');
Route::get('/online_islemler','MainController@online_islemler');
Route::get('/blog','MainController@blog');




Route::get('/login','MainController@login')->name('login');
Route::post('/logmein','MainController@logmein');
Route::get('logout','MainController@logout');


Route::get('sac_ekimi','AdminController@sac_ekimi');
Route::post('sacekimi','AdminController@sacekimi')->name('sacekimpost');
Route::post('sacekimi_cat','AdminController@sacekimi_cat')->name('sacekimcat');

Route::get('sakal_biyik','AdminController@sakal_biyik');
Route::post('sakalbiyik','AdminController@sakalbiyik')->name('sakalbiyikpost');
Route::post('sakalbiyik_cat','AdminController@sakalbiyik_cat')->name('sakalbiyik_cat');

Route::get('sac_dokulme_tedavi','AdminController@sac_dokulme_tedavi');
Route::post('sacdokulme_tedavi','AdminController@sacdokulmetedavi')->name('sacdokulmepost');
Route::post('sacdokulmetedavi_cat','AdminController@sacdokulmetedavi_cat')->name('sacdokulmetedavi_cat');

Route::get('kas_ekimi','AdminController@kas_ekimi');
Route::post('kasekimi','AdminController@kasekimi')->name('kasekimi');
Route::post('kasekimi_cat','AdminController@kasekimi_cat')->name('kasekimi_cat');

Route::get('oncesi_sonrasi','AdminController@oncesi_sonrasi')->name('oncesi_sonrasi');
Route::post('oncesisonrasi','AdminController@oncesisonrasi')->name('oncesisonrasi');
Route::post('oncesisonrasi_cat','AdminController@oncesisonrasi_cat')->name('oncesisonrasi_cat');

Route::get('sosyal_medya','AdminController@sosyal_medya');
Route::post('sosyalmedya','AdminController@sosyalmedya')->name('socialpost');

Route::get('blog_post','AdminController@blog')->name('blog.post');
Route::get('randevular','AdminController@randevular')->name('appointments');
Route::get('mesajlar','AdminController@mesajlar')->name('messages');

Route::get('/admin','AdminController@panel');



